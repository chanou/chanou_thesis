#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np

def deriveDagger(operator):
  op_dagger = np.conjugate(operator.T)

  return op_dagger

# rotate dm
def rotateDm(operator, dm):
  operator_d = deriveDagger(operator)
  process = np.dot(operator, dm)
  dm_r = np.dot(process, operator_d)

  return dm_r

def defineState(state, bit):
  if state == '+' and bit == '0':
    state = '↑'
  elif state == '+' and bit == '1':
    state = '↓'
  elif state == 'X' and bit == '0':
    state = '↗︎'
  elif state == 'X' and bit == '1':
    state = '↙︎'

  return state

# derive probability corresponding to measurement result
def deriveProb(mes, dm):
  process = np.dot(deriveDagger(mes), mes)
  process = np.dot(process, dm)
  # FIXME:roundは四捨五入, realは実部のみ取得 <- 複素数は含まれないはず
  prob = np.trace(process).real

  return prob
