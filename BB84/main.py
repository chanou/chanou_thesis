#!/usr/bin/env python
# -*- coding: utf-8 *-

import numpy as np
import random
import fixedVecs as fiv
import vec_methods as vmes
import dm_mehtods as dm_mes
from matplotlib import pyplot

def BB84(bits, fidelity):
  secretkey = []
  results = []
  success_rate = 0
  for i in range(bits):
    alice = {}
    bob = {}

    # define dm of qubit to send
    qubit = str(random.randrange(2))
    # noise must be added after send
    dm = fiv.dm_I[qubit]

    # define send basis
    alice["basis"] = np.random.choice(
      # +: Vertical X: Slanting
      ['+', 'X'],
      p = [0.5, 0.5]
      )
    
    alice["rotateOp"] = fiv.definerotateOp(alice["basis"])

    rotatedDm = vmes.rotateDm(alice["rotateOp"], dm)

    # add noises
    # should be dm corresponding to sended qubit
    rotatedDm = dm_mes.defineDM(qubit, fidelity, rotatedDm)
    # print("aliceのノイズ混入後のdmの確認\n", rotatedDm)

    alice["state"] = vmes.defineState(alice["basis"], qubit)

    # define measurement basis
    bob["basis"] = np.random.choice(
      ['+','X'],
      p = [0.5, 0.5]
      )
    
    bob["rotateOp"] = fiv.definerotateOp(bob["basis"])

    rotatedDm = vmes.rotateDm(bob["rotateOp"], rotatedDm)

    probs = {
      "P0": vmes.deriveProb(fiv.mesOps['0'], rotatedDm),
      "P1": vmes.deriveProb(fiv.mesOps['1'], rotatedDm),
    }
    # print("probsの確認", probs)

    result = np.random.choice(
      ['0', '1'],
      p = [
        probs["P0"],
        probs["P1"],
      ]
    )

    bob["state"] = vmes.defineState(bob["basis"], result)

    results.append(
      {
        "alice bit": qubit,
        "alice base": alice["basis"],
        "alice state": alice["state"],
        "bob base": bob["basis"],
        "bob state": bob["state"],
        "bob bit": result
      }
    )

    print(i + 1, "bit", results[i], end="")

    if (results[i]["alice bit"] == results[i]["bob bit"]) and (results[i]["alice base"] == results[i]["bob base"]):
      secretkey.append(int(results[i]["alice bit"]))
      print(': ◯')
    else:
      print(': ×')

  print("\n=======>secret key")
  print(secretkey)

  print("\n=======>success rate")
  success_rate = len(secretkey) / bits
  print(success_rate, "\n")

  return success_rate

if __name__ == "__main__":
  error_rates = []
  success_rates = []

  print("\n#####This is BB84 sim by chanou#####")
  print("bits? ", end = "")
  bits = int(input())

  # 一つだけfidelityを変化させる
  while True:
    print("Which error to change?", end="")
    target_fid = input()

    if (target_fid == 'X') or (target_fid == 'Y') or (target_fid == 'Z'):
      break

  print("How many error to change?", end="")
  fid_change_value = int(input())
  
  for i in range(int(100/fid_change_value)):
    fidelity = {
      'I': 0,
      'X': 0,
      'Y': 0,
      'Z': 0,
    }
    
    print(i * fid_change_value)
    error_rate = float(i * fid_change_value) / 100
    if target_fid == 'X':
      fidelity = {
        'I': 1 - error_rate,
        'X': error_rate,
        'Y': 0,
        'Z': 0,
      }
    elif target_fid == 'Y':
      fidelity = {
        'I': 1 - error_rate,
        'X': 0,
        'Y': error_rate,
        'Z': 0,
      }
    elif target_fid == 'Z':
      fidelity = {
        'I': 1 - error_rate,
        'X': 0,
        'Y': 0,
        'Z': error_rate,
      }

    print(fidelity)
    success_rate = BB84(bits, fidelity)

    error_rates.append(error_rate)
    success_rates.append(success_rate)

  print(error_rates)
  print(success_rates)

  # Display angles and S_values
  x = error_rates
  y = success_rates

  pyplot.plot(x, y)
  pyplot.show()

#TODO: イブの実装
