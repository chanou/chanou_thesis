#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import random
import vec_methods as vmes

# Display up to two decimal places
# np.set_printoptions(precision=2)

# Display decimal, NOT exponential
np.set_printoptions(suppress=True)

calBase = {
  '0': np.array([1, 0]).reshape(2, 1),
  '1': np.array([0, 1]).reshape(2, 1),
}

mesOps = {
  '0': np.dot(calBase['0'], vmes.deriveDagger(calBase['0'])),
  '1': np.dot(calBase['1'], vmes.deriveDagger(calBase['1'])),
}

# pauli gate
I = np.array([
  [1, 0],
  [0, 1]
])

X = np.array([
  [0, 1],
  [1, 0]
])

Y = np.array([
  [0, -1j],
  [1j, 0]
])

Z = np.array([
  [1, 0],
  [0, -1]
])

# DM added noise
dm_I = {
  '0': np.dot(calBase['0'], vmes.deriveDagger(calBase['0'])),
  '1': np.dot(calBase['1'], vmes.deriveDagger(calBase['1'])),
}

dm_X= {
  '0': np.dot(np.dot(X, dm_I['0']), vmes.deriveDagger(X)),
  '1': np.dot(np.dot(X, dm_I['1']), vmes.deriveDagger(X)),
}

dm_Y = {
  '0': np.dot(np.dot(Y, dm_I['0']), vmes.deriveDagger(Y)),
  '1': np.dot(np.dot(Y, dm_I['1']), vmes.deriveDagger(Y))
}

dm_Z = {
  '0': np.dot(np.dot(Z, dm_I['0']), vmes.deriveDagger(Z)),
  '1': np.dot(np.dot(Z, dm_I['1']), vmes.deriveDagger(Z))
}

def definerotateOp(basis):
  if basis == 'X':
    rotateOp = np.array([
      [1/np.sqrt(2), 1/np.sqrt(2)],
      [1/np.sqrt(2), -1/np.sqrt(2)]
    ])
  elif basis == '+':
    rotateOp = np.array([
      [1, 0],
      [0, 1]
    ])

  return rotateOp
