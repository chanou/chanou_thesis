#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import fixedVecs as fiv

def defineFid():
  while True:
    print("I fidelity? ", end="")
    I = float(input())

    print("X fidelity? ", end="")
    X = float(input())

    print("Y fidelity? ", end="")
    Y = float(input())
  
    print("Z fidelity? ", end="")
    Z = float(input())

    if I + X + Y + Z == 1.0:
      fidelity = {
        'I': I,
        'X': X,
        'Y': Y,
        'Z': Z,
      }
      return fidelity
      break

def defineDM(qubit, fidelity, dm):
  dm_actual = np.dot(fidelity['I'], fiv.dm_I[qubit]) + np.dot(fidelity['X'], fiv.dm_X[qubit]) + np.dot(fidelity['Y'], fiv.dm_Y[qubit]) + np.dot(fidelity['Z'], fiv.dm_Z[qubit])

  return dm_actual
