#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import methods as mt
import E91 as E91
from matplotlib import pyplot
# from dm_methods import Dm 

# Display up two decimal places
# np.set_printoptions(precision=2)

# Display decimal, NOT exponential
np.set_printoptions(suppress=True)

if __name__ == "__main__":
  error_rates = []
  eve_rates = []
  success_rates = []
  s_values = []
  
  print('\n#####This is E91 sim by chanou#####')

  bits = mt.defBits()

  # TODO: まずは途中で測定して古典相関にしてみる
  eve_flag = mt.defEveflag()
  eve_change_rate = mt.defEverate(eve_flag)

  # 一つだけfidelityを変化させる
  target_fid = mt.selectError()
  fid_change_value = mt.defFidChangeValue()

  # In case of no noise
  if fid_change_value == 0:
    # ノイズ：存在しない　eve：存在しない
    if eve_change_rate == 0:
      error_rate = float(fid_change_value) / 100
      fidelity = mt.defFid(target_fid, fid_change_value, error_rate)

      success_rate = E91.E91(bits, fidelity, target_fid, error_rate, eve_rate)
      print(success_rate)

    # ノイズ：存在しない　eve：存在する
    else:
      error_rate = float(fid_change_value) / 100
      fidelity = mt.defFid(target_fid, fid_change_value, error_rate)

      for i in range(int(100/eve_change_rate)):
        eve_rate = float(i * eve_change_rate) / 100
        print('*********eve_rate', eve_rate)
        results = E91.E91(bits, fidelity, target_fid, error_rate, eve_rate)

        eve_rates.append(eve_rate)
        s_values.append(results["S_value"])

      print(eve_rates)
      print(s_values)

      # Display angles and S_values
      x = eve_rates
      # y = success_rates
      y = s_values

      pyplot.plot(x, y)
      pyplot.show()

  # In case of there is noise
  else:
    eve_rate = False
    # ノイズ：存在する　eve：存在しない
    if eve_rate == 'y':
      print('ぴぃあ')
    # ノイズ：存在する　eve：存在する
    else:
      for i in range(int(100/fid_change_value)):
        error_rate = float(i * fid_change_value) / 100
        fidelity = mt.defFid(target_fid, fid_change_value, error_rate)
        print("=======>fidelity\n", fidelity)

        # success_rate = E91.E91(bits, fidelity)
        results = E91.E91(bits, fidelity, target_fid, error_rate, eve_rate)

        error_rates.append(error_rate)
        success_rates.append(results["success_rate"])
        s_values.append(results["S_value"])

    print(error_rates)
    print(success_rates)
    print(s_values)

    # Display angles and S_values
    x = error_rates
    # y = success_rates
    y = s_values

    pyplot.plot(x, y)
    pyplot.show()

#TODO: イブの実装
