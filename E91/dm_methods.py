#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import random
import sys
from pprint import pprint 

np.set_printoptions(precision=2)

class Dm:
  def __init__(self):
    self.bellpairs = {
      # TODO:reshape()について
      # 今までの実装は転置されていなかったぽい
      'Phi_plus': (1/np.sqrt(2)) * np.array([1,0,0,1]).reshape((1,4)).T,
      # 'Phi_plus' : (1/np.sqrt(2)) * np.array([1, 0, 0, 1]),
      'Phi_minus' : (1/np.sqrt(2)) * np.array([1, 0, 0, -1]),
      'Psi_plus' : (1/np.sqrt(2)) * np.array([0, 1, 1, 0]),
      'Psi_minus' : (1/np.sqrt(2)) * np.array([0, 1, -1,0])
    }

  def define_bellpair(self, bell_name):
    return self.bellpairs[bell_name]

  def define_dm(self, bellpair):
    dm = np.dot(bellpair, np.conjugate(bellpair.T))
    return dm

  # TODO: targetを任意に変更する
  def test(self, flag, ali_angle, bob_angle):
    # まずはPhi_plusで実行
    target = self.bellpairs['Phi_plus']
    target_t = target.T
    target_d = np.conjugate(target_t)
    print('target',target)
    print('target_t',target_t)
    print('target_d',target_d)

    dm = np.dot(target, target_d)
    print('測定前のdm')
    print(dm)

    # 回転行列の実装
    # TODO:マイナス方向に回転するため、daggerにする？ ->minusにした
    # theta = -0
    # theta = -90
    # theta = -45
    # theta = -135

    # theta = -0
    # theta = -45
    # theta = -60
    # print('_____cosの確認',np.cos(np.radians(theta/2)))

    # r_x = np.array([
    #         [np.cos(np.radians(theta/2)), -1j*np.sin(np.radians(theta/2))],
    #         [-1j*np.sin(np.radians(theta/2)), np.cos(np.radians(theta/2))]
    #         ])

    # TODO:ここの角度は1/2なのか1/4なのかを確実にすること -> qubitに対しては1/2でいいかも？
    ali_r_x = np.array([
            [np.cos(np.radians(ali_angle/2)), -1j*np.sin(np.radians(ali_angle/2))],
            [-1j*np.sin(np.radians(ali_angle/2)), np.cos(np.radians(ali_angle/2))]
            ])

    bob_r_x = np.array([
            [np.cos(np.radians(bob_angle/2)), -1j*np.sin(np.radians(bob_angle/2))],
            [-1j*np.sin(np.radians(bob_angle/2)), np.cos(np.radians(bob_angle/2))]
            ])

    ali_r_x = np.conjugate(ali_r_x.T)
    bob_r_x = np.conjugate(bob_r_x.T)

    print('____ali_r_xについて\n')
    print(ali_r_x)

    print('____bob_r_xについて\n')
    print(bob_r_x)

    r_x = np.kron(ali_r_x, bob_r_x)

    # ali_r_x_d = np.conjugate(ali_r_x.T)
    # bob_r_x_d = np.conjugate(bob_r_x.T)

    # print('____ali_r_x_dについて\n')
    # print(ali_r_x_d)

    # print('____bob_r_x_dについて\n')
    # print(bob_r_x_d)
    
    r_x_d = np.conjugate(np.kron(ali_r_x, bob_r_x).T)

    print('r_xの++++確認++++')
    print(r_x)
    print('r_x_dの++++確認++++')
    print(r_x_d)

    dm = np.dot(r_x, dm)
    dm = np.dot(dm, np.conjugate(r_x_d.T))

    print('回転後のdm')
    print(dm)

    # TODO: dmに対し、fidelityに対するノイズを加える

    # |0>に対応するZ規定の時の測定オペレータの作成
    mes_0 = np.array([[1,0],[0,0]])
    # |1>に対応するZ規定の時の測定オペレータの作成
    mes_1 = np.array([[0,0],[0,1]])

    M_00 = np.kron(mes_0, mes_0)
    M_01 = np.kron(mes_0, mes_1)
    M_10 = np.kron(mes_1, mes_0)
    M_11 = np.kron(mes_1, mes_1)

    # M_00_d = np.kron(np.conjugate(mes_0.T), np.conjugate(mes_0.T))
    # M_01_d = np.kron(np.conjugate(mes_0.T), np.conjugate(mes_1.T))
    # M_10_d = np.kron(np.conjugate(mes_1.T), np.conjugate(mes_0.T))
    # M_11_d = np.kron(np.conjugate(mes_1.T), np.conjugate(mes_1.T))

    M_00_d = np.conjugate(np.kron(mes_0, mes_0))
    M_01_d = np.conjugate(np.kron(mes_0, mes_1)) 
    M_10_d = np.conjugate(np.kron(mes_1, mes_0)) 
    M_11_d = np.conjugate(np.kron(mes_1, mes_1))

    if flag == "a_0b_0":
      result = np.dot(M_00_d, M_00)
      result = np.dot(result, dm)
    elif flag == "a_0b_1":
      result = np.dot(M_01_d, M_01)
      result = np.dot(result, dm)
    elif flag == "a_1b_0":
      result = np.dot(M_10_d, M_10)
      result = np.dot(result, dm)
    elif flag == "a_1b_1":
      result = np.dot(M_11_d, M_11)
      result = np.dot(result, dm)

    print('測定後のdm',result)
    print('得られる確率について', result.trace())
    
    return result.trace()

    # rho = 1/2 * np.array([
    #               [1,0,0,1],
    #               [0,0,0,0],
    #               [0,0,0,0],
    #               [1,0,0,1]
    #             ])

    # # X = np.array([[0,1],[1,0]])
    # Xp = np.array([
    #   [(1+0)/2,(0+1)/2],
    #   [(0+1)/2,(0+0)/2]
    #   ])
    # Xm = np.array([
    #   [(1-0)/2,(0-1)/2],
    #   [(0-1)/2,(0-0)/2]
    #   ])
    # # M_x = np.kron(Xp,Xp)
    # M_x = np.kron(Xm,Xm)
    # M_x_dagger = np.conjugate(M_x.T)
    # print('M_x',M_x)
    # print('M_x_dagger',M_x_dagger)
    # prob_x = np.dot(M_x_dagger, M_x)
    # prob_x = np.dot(prob_x, rho)
    # print('prob_x',prob_x)
    # print('prob_x',prob_x.trace())

    # Zでもやる
    # X = np.array([[0,1],[1,0]])
    # M_x = np.kron(X,X)
    # M_x_dagger = np.conjugate(np.kron(X,X).T)
    # print('M_x',M_x)
    # print('M_x_dagger',M_x_dagger)
    # prob_x = np.dot(M_x_dagger, M_x)
    # prob_x = np.dot(prob_x, rho)
    # print('prob_x',prob_x)
    # print('prob_x',prob_x.trace())

    # M_00 =  np.kron(zero, zero)
    # M_01 =  np.kron(zero, one)
    # M_10 =  np.kron(one, zero)
    # M_11 =  np.kron(one, one)

    # # print('M_00',M_00)


    # prob_00 = np.dot(M_00,M_00)
    # prob_00 = np.dot(M_00,rho)

    # prob_01 = np.dot(M_01,M_01)
    # prob_01 = np.dot(M_01,rho)


    # prob_10 = np.dot(M_10,M_10)
    # prob_10 = np.dot(M_10,rho)

    # prob_11 = np.dot(M_11,M_11)
    # prob_11 = np.dot(M_11,rho)

    # # print('prob_00',prob_00.trace())
    # # print('prob_01',prob_01.trace())
    # # print('prob_10',prob_10.trace())
    # # print('prob_11',prob_11.trace())





    # return result
  
  # FIXME：なんか小さい少数点の値がおかしい
  def set_fidelity(self):
    while True:
      print('Fidelity?: ', end="")
      f_of_bell = int(input())/100
      if f_of_bell <= 1:
        break
      print('error <fidelity of bell pair>: ', f_of_bell, '\n')

    return f_of_bell

  # !!!!!いまはXとかに対応する行列がわからない!!!!!
  # どのようにerrorが発生するのか実装
  def set_error_type(self):
    # types = [
    #   ['X'],
    #   ['Y'],
    #   ['Z'],
    #   ['X', 'Y'],
    #   ['X', 'Z'],
    #   ['Y', 'Z'],
    #   ['X', 'Y', 'Z']
    # ]
    error_type = types[random.randrange(6)]

    return error_type

  def set_error_rate(self, f_of_bell):
    while True:
      remaining_error = 1 - f_of_bell
      print('(TOTAL ERROR RATE: ', remaining_error, ")")
      # x error
      while True:
        print('x-error(Phase error) rate?: ', end="")
        x_error = int(input())/100
        if x_error < remaining_error:
          break
        print('error <phase error rate>: ', x_error, '\n')

      # z error
      remaining_error = remaining_error - x_error
      print('(REMANING ERROR RATE: ', remaining_error, ")")
      while True:
        print('z-error(Bit-flip error) rate?: ', end="")
        z_error = int(input())/100
        if z_error < remaining_error:
          break
        print('error <bit-flip error rate>: ', z_error, '\n')

      # y error
      # FIXME:切り捨てが必要ないようにしたい
      y_error = round(remaining_error - z_error, 2)
      if x_error + y_error + z_error + f_of_bell == 1:
        break
      print('error <error rate>: ', x_error + y_error + z_error, 1 - f_of_bell, '\n')

    return [
      x_error,
      y_error,
      z_error
    ]

  def select_bellpair(self):
    index_bellpairs = [
      'Phi_plus',
      'Phi_minus',
      'Psi_plus',
      'Psi_minus'
    ]
    i = random.randrange(4)

    return [
      [
        index_bellpairs[i],
        self.bellpairs[index_bellpairs[i]]
      ]
    ]
    
  # !!!TODO:redundant!!!
  def set_ensemble(self, q_state):
    if q_state[0][0] == 'Phi_plus':
      q_state.append([
        'Psi_plus',
        self.bellpairs['Psi_plus']
      ])
      q_state.append([
        'Psi_minus',
        self.bellpairs['Psi_minus']
      ])
      q_state.append([
        'Phi_minus',
        self.bellpairs['Phi_minus']
      ])
    elif q_state[0][0] == 'Phi_minus':
      q_state.append([
        'Psi_minus',
        self.bellpairs['Psi_minus']
      ])
      q_state.append([
        'Psi_plus',
        self.bellpairs['Psi_plus']
      ])
      q_state.append([
        'Phi_plus',
        self.bellpairs['Phi_plus']
      ])
    elif q_state[0][0] == 'Psi_plus':
      q_state.append([
        'Phi_plus',
        self.bellpairs['Phi_plus']
      ])
      q_state.append([
        'Phi_minus',
        self.bellpairs['Psi_minus']
      ])
      q_state.append([
        'Psi_minus',
        self.bellpairs['Phi_minus']
      ])
    elif q_state[0][0] == 'Psi_minus':
      q_state.append([
        'Phi_minus',
        self.bellpairs['Phi_minus']
      ])
      q_state.append([
        'Phi_plus',
        self.bellpairs['Phi_plus']
      ])
      q_state.append([
        'Psi_plus',
        self.bellpairs['Psi_plus']
      ])
    else:
      print('error <set ensemble>: ', q_state, '\n')
      sys.exit()

    return q_state

  # !!!TODO:redundant!!!
  def convert_to_dm(self, ensemble, f_of_bell, error_rate):
    # TODO:selfで実装していいかも
    p_A = f_of_bell
    p_B = error_rate[0]
    p_C = error_rate[1]
    p_D = error_rate[2]
    state_A = np.matrix(ensemble[0][1])
    state_B = np.matrix(ensemble[1][1])
    state_C = np.matrix(ensemble[2][1])
    state_D = np.matrix(ensemble[3][1])
    dm = p_A * np.dot(state_A.T, state_A) + p_B * np.dot(state_B.T, state_B) + p_C * np.dot(state_C.T, state_C) + p_D * np.dot(state_D.T, state_D)

    return dm

if __name__ == '__main__':
  print("I'm dm_methds.py, and the code itself was called")
