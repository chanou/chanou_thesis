#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import vec_methods as vmes

# Display up to two decimal places
np.set_printoptions(precision=2)

# Display decima, NOT exponential
# np.set_printoptions(suppress=True)

# density matrix
# TODO: dm_methods.pyで実装
# TODO:式を定義し、導出する
# TODO:ノイズの混合の実装
dm_I = np.array([
    [0.5, 0, 0, 0.5],
    [0, 0, 0, 0],
    [0, 0, 0, 0],
    [0.5, 0, 0, 0.5],
  ])

# もつれていないパター完全混合
# dm = np.array([
#   [0.5, 0, 0, 0],
#   [0, 0, 0, 0],
#   [0, 0, 0, 0],
#   [0, 0, 0, 0.5],
# ])

dm_X = np.array([
  [0, 0, 0, 0],
  [0, 0.5, 0.5, 0],
  [0, 0.5, 0.5, 0],
  [0, 0, 0, 0],
])

dm_Z = np.array(
  [
    [0.5, 0, 0, -0.5],
    [0, 0, 0, 0],
    [0, 0, 0, 0],
    [-0.5, 0, 0, 0.5],
  ]
)

dm_Y = np.array([
  [0, 0, 0, 0],
  [0, 0.5, -0.5, 0],
  [0, -0.5, 0.5, 0],
  [0, 0, 0, 0],
])

# caluculation base
calBase = {
  '0': np.array([1 , 0]).reshape(2, 1),
  '1': np.array([0, 1]).reshape(2, 1)
  }

# Z measurement operator
mesOps = {
  # ここを変更した
  # '0': vmes.deriveZMesOp(calBase['0']),
  # '1': vmes.deriveZMesOp(calBase['1'])
  '0': np.dot(calBase['0'], vmes.deriveDagger(calBase['0'])),
  '1': np.dot(calBase['1'], vmes.deriveDagger(calBase['1'])),
}

def rotateY(theta, vec):
  rotate = np.array([
    [np.cos(theta/2), -np.sin(theta/2)],
    [np.sin(theta/2), np.cos(theta/2)]
  ])
  rotatedVec = np.dot(rotate, vec)

  return rotatedVec

def deriveRotateBsOp(theta,vec):
  theta_minus = -(np.pi - theta)
  plusvec = rotateY(theta, calBase["0"])
  minusvec = rotateY(theta_minus, calBase["0"])
  A = plusvec[0][0]
  B = plusvec[1][0]
  Ad = minusvec[0][0]
  Bd = minusvec[1][0]

  rotateBaseOp = np.array([
    [-Bd / (Ad * B - A * Bd), Ad / (Ad * B - A * Bd)],
    [- B / (A * Bd - Ad * B), A / (A * Bd -  Ad * B)]
  ])

  return rotateBaseOp
  
# implement as a function, for the case of dm with errors
def defineRotateBsOp(dm_actual):
  rotatedDms = {
    "AB": vmes.rotateDm(rotateBaseOps["AB"], dm_actual),
    "Ab": vmes.rotateDm(rotateBaseOps["Ab"], dm_actual),
    "AA": vmes.rotateDm(rotateBaseOps["AA"], dm_actual),
    "aB": vmes.rotateDm(rotateBaseOps["aB"], dm_actual),
    "ab": vmes.rotateDm(rotateBaseOps["ab"], dm_actual),
    "aA": vmes.rotateDm(rotateBaseOps["aA"], dm_actual),
    "BB": vmes.rotateDm(rotateBaseOps["BB"], dm_actual),
    "Bb": vmes.rotateDm(rotateBaseOps["Bb"], dm_actual),
    "BA": vmes.rotateDm(rotateBaseOps["BA"], dm_actual)
  }

  return rotatedDms

# rotation operator
rotateOps = {
  # A(alice) 0度(Z測定)
  'A': np.array([
    [1, 0],
    [0, 1]
  ]),
  # a(alice) 90度(X測定)
  'a': deriveRotateBsOp(np.pi/2, calBase["0"]),
  # B(bob) 45度( (Z+X)/\sqrt(2) )
  # 'B': deriveRotateBsOp(np.pi/4, calBase["0"]),
  'B': np.array([
      [(1 + np.exp(1j * np.pi/4))/2, 1j * (1 - np.exp(1j * np.pi/4))/2],
      [(1 - np.exp(1j * np.pi/4))/2, 1j * (1 + np.exp(1j * np.pi/4))/2]
      ]),
  # b(bob) 135度( (Z-X)/\sqrt(2) )
  # 'b': deriveRotateBsOp(3 * np.pi/4, calBase["0"]),
  'b': np.array([
      [(1 + np.exp(-1j * np.pi/4))/2, 1j * (1 - np.exp(-1j * np.pi/4))/2],
      [(1 - np.exp(-1j * np.pi/4))/2, 1j * (1 + np.exp(-1j * np.pi/4))/2]
      ]),
  }

# rotate BASE operator
rotateBaseOps = {
    "AB": vmes.deriveRotateBsOp(rotateOps['A'], rotateOps['B']),
    "Ab": vmes.deriveRotateBsOp(rotateOps['A'], rotateOps['b']),
    "AA": vmes.deriveRotateBsOp(rotateOps['A'], rotateOps['A']),
    "aB": vmes.deriveRotateBsOp(rotateOps['a'], rotateOps['B']),
    "ab": vmes.deriveRotateBsOp(rotateOps['a'], rotateOps['b']),
    "aA": vmes.deriveRotateBsOp(rotateOps['a'], rotateOps['A']),
    "BB": vmes.deriveRotateBsOp(rotateOps['B'], rotateOps['B']),
    "Bb": vmes.deriveRotateBsOp(rotateOps['B'], rotateOps['b']),
    "BA": vmes.deriveRotateBsOp(rotateOps['B'], rotateOps['A'])
  }
