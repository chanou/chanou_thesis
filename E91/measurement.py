#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np

def deriveS(results, target_fid, error_rate):
  total_AB = 0
  s_AB_00 = 0
  s_AB_01 = 0
  s_AB_10 = 0
  s_AB_11 = 0

  total_Ab = 0
  s_Ab_00 = 0
  s_Ab_01 = 0
  s_Ab_10 = 0
  s_Ab_11 = 0

  total_AA = 0
  s_AA_00 = 0
  s_AA_01 = 0
  s_AA_10 = 0
  s_AA_11 = 0

  total_aB = 0
  s_aB_00 = 0
  s_aB_01 = 0
  s_aB_10 = 0
  s_aB_11 = 0

  total_ab = 0
  s_ab_00 = 0
  s_ab_01 = 0
  s_ab_10 = 0
  s_ab_11 = 0

  total_aA = 0
  s_aA_00 = 0
  s_aA_01 = 0
  s_aA_10 = 0
  s_aA_11 = 0

  total_BB = 0
  s_BB_00 = 0
  s_BB_01 = 0
  s_BB_10 = 0
  s_BB_11 = 0

  total_Bb = 0
  s_Bb_00 = 0
  s_Bb_01 = 0
  s_Bb_10 = 0
  s_Bb_11 = 0

  total_BA = 0
  s_BA_00 = 0
  s_BA_01 = 0
  s_BA_10 = 0
  s_BA_11 = 0

  for i in range(len(results)):

    if results[i]["base"] == "AB":
      if results[i]["result"] == "00":
        total_AB = total_AB + 1
        s_AB_00 = s_AB_00 + 1
      elif results[i]["result"] == "01":
        total_AB = total_AB + 1
        s_AB_01 = s_AB_01 - 1
      elif results[i]["result"] == "10":
        total_AB = total_AB + 1
        s_AB_10 = s_AB_10 - 1
      elif results[i]["result"] == "11":
        total_AB = total_AB + 1
        s_AB_11 = s_AB_11 + 1

    elif results[i]["base"] == "Ab":
      if results[i]["result"] == "00":
        total_Ab = total_Ab + 1
        s_Ab_00 = s_Ab_00 + 1
      elif results[i]["result"] == "01":
        total_Ab = total_Ab + 1
        s_Ab_01 = s_Ab_01 - 1
      elif results[i]["result"] == "10":
        total_Ab = total_Ab + 1
        s_AB_10 = s_AB_10 - 1
      elif results[i]["result"] == "11":
        total_Ab = total_Ab + 1
        s_Ab_11 = s_Ab_11 + 1

    # elif results[i]["base"] == "AA":
    #   if results[i]["result"] == "00":
    #     total_AA = total_AA + 1
    #     s_AA_00 = s_AA_00 + 1
    #   elif results[i]["result"] == "01":
    #     total_AA = total_AA + 1
    #     s_AA_01 = s_AA_01 - 1
    #   elif results[i]["result"] == "10":
    #     total_AA = total_AA + 1
    #     s_AB_10 = s_AB_10 - 1
    #   elif results[i]["result"] == "11":
    #     total_AA = total_AA + 1
    #     s_AA_11 = s_AA_11 + 1

    elif results[i]["base"]  == "aB":
      if results[i]["result"] == "00":
        total_aB = total_aB + 1
        s_aB_00 = s_aB_00 + 1
      elif results[i]["result"] == "01":
        total_aB = total_aB + 1
        s_aB_01 = s_aB_01 - 1
      elif results[i]["result"] == "10":
        total_aB = total_aB + 1
        s_aB_10 = s_aB_10 - 1
      elif results[i]["result"] == "11":
        total_aB = total_aB + 1
        s_aB_11 = s_aB_11 + 1

    elif results[i]["base"]  == "ab":
      if results[i]["result"] == "00":
        total_ab = total_ab + 1
        s_aB_00 = s_aB_00 + 1
      elif results[i]["result"] == "01":
        total_ab = total_ab + 1
        s_ab_01 = s_ab_01 - 1
      elif results[i]["result"] == "10":
        total_ab = total_ab + 1
        s_ab_10 = s_ab_10 - 1
      elif results[i]["result"] == "11":
        total_ab = total_ab + 1
        s_ab_11 = s_ab_11 + 1

    elif results[i]["base"]  == "aA":
      if results[i]["result"] == "00":
        total_aA = total_aA + 1
        s_aA_00 = s_aA_00 + 1
      elif results[i]["result"] == "01":
        total_aA = total_aA + 1
        s_aA_01 = s_aA_01 - 1
      elif results[i]["result"] == "10":
        total_aA = total_aA + 1
        s_aA_10 = s_aA_10 - 1
      elif results[i]["result"] == "11":
        total_aA = total_aA + 1
        s_aA_11 = s_aA_11 + 1

    # elif results[i]["base"] == "BB":
    #   if results[i]["result"] == "00":
    #     total_BB = total_BB + 1
    #     s_BB_00 = s_BB_00 + 1
    #   elif results[i]["result"] == "01":
    #     total_BB = total_BB + 1
    #     s_BB_01 = s_BB_01 - 1
    #   elif results[i]["result"] == "10":
    #     total_BB = total_BB + 1
    #     s_BB_10 = s_BB_10 - 1
    #   elif results[i]["result"] == "11":
    #     total_BB = total_BB + 1
    #     s_BB_11 = s_BB_11 + 1

    elif results[i]["base"] == "Bb":
      if results[i]["result"] == "00":
        total_Bb = total_Bb + 1
        s_Bb_00 = s_Bb_00 + 1
      elif results[i]["result"] == "01":
        total_Bb = total_Bb + 1
        s_Bb_01 = s_Bb_01 - 1 
      elif results[i]["result"] == "10":
        total_Bb = total_Bb + 1
        s_Bb_10 = s_Bb_10 - 1
      elif results[i]["result"] == "11":
        total_Bb = total_Bb + 1
        s_Bb_11 = s_Bb_11 + 1

    elif results[i]["base"] == "BA":
      if results[i]["result"] == "00":
        total_BA = total_BA + 1
        s_BA_00 = s_BA_00 + 1
      elif results[i]["result"] == "01":
        total_BA = total_BA + 1
        s_BA_01 = s_BA_01 - 1 
      elif results[i]["result"] == "10":
        total_BA = total_BA + 1
        s_BA_10 = s_BA_10 - 1
      elif results[i]["result"] == "11":
        total_BA = total_BA + 1
        s_BA_11 = s_BA_11 + 1

  print("\n<=======vars for deriving S value=======>")
  print("total AB: ", total_AB)
  print("total Ab: ", total_Ab)
  # print("total AA: ", total_AA)
  print("total aB: ", total_aB)
  print("total ab: ", total_ab)
  print("total aA: ", total_aA)
  # print("total BB: ", total_BB)
  print("total Bb: ", total_Bb)
  print("total BA: ", total_BA)
  # print("trial count: ", total_AB + total_Ab + total_AA + total_aB + total_ab + total_aA + total_BB + total_Bb + total_BA)
  print("trial count: ", total_AB + total_Ab + total_aB + total_ab + total_aA + total_Bb + total_BA)

  print("\n-AB", total_AB, s_AB_00, s_AB_01, s_AB_10, s_AB_11)
  cor_AB = (s_AB_00 + s_AB_01 + s_AB_10 + s_AB_11) / total_AB
  print("<AB>", cor_AB)

  print("-Ab", total_Ab, s_Ab_00, s_Ab_01, s_Ab_10, s_Ab_11)
  cor_Ab = (s_Ab_00 + s_Ab_01 + s_Ab_10 + s_Ab_11) / total_Ab
  print("<Ab>", cor_Ab)

  # print("-AA", total_AA, s_AA_00, s_AA_01, s_AA_10, s_AA_11)
  # cor_AA = (s_AA_00 + s_AA_01 + s_AA_10 + s_AA_11) / total_AA
  # print("<AA>", cor_AA)

  print("\n-aB", total_aB, s_aB_00, s_aB_01, s_aB_10, s_aB_11)
  cor_aB = (s_aB_00 + s_aB_01 + s_aB_10 + s_aB_11) / total_aB
  print("<aB>", cor_aB)

  print("-ab", total_ab, s_ab_00, s_ab_01, s_ab_10, s_ab_11)
  cor_ab = (s_ab_00 + s_ab_01 + s_ab_10 + s_ab_11) / total_ab
  print("<ab>", cor_ab)

  print("-aA", total_aA, s_aA_00, s_aA_01, s_aA_10, s_aA_11)
  cor_aA = (s_aA_00 + s_aA_01 + s_aA_10 + s_aA_11) / total_aA
  print("<aA>", cor_aA)

  # print("\n-BB", total_BB, s_BB_00, s_BB_01, s_BB_10, s_BB_11)
  # cor_BB = (s_BB_00 + s_BB_01 + s_BB_10 + s_BB_11) / total_BB
  # print("<BB>", cor_BB)

  print("-Bb", total_Bb, s_Bb_00, s_Bb_01, s_Bb_10, s_Bb_11)
  cor_Bb = (s_Bb_00 + s_Bb_01 + s_Bb_10 + s_Bb_11) / total_Bb
  print("<Bb>", cor_Bb)

  print("-BA", total_BA, s_BA_00, s_BA_01, s_BA_10, s_BA_11)
  cor_BA = (s_BA_00 + s_BA_01 + s_BA_10 + s_BA_11) / total_BA
  print("<BA>", cor_BA)

  print("\n<=======S value=======>")
  # Alice: A,a,B
  # Bob: B,b,A
  print("<AB>", cor_AB)
  print("<Ab>", cor_Ab)
  # print("<AA>", cor_AA)

  print("<aB>", cor_aB)
  print("<ab>", cor_ab)
  print("<aA>", cor_aA)
  print("<Bb>", cor_Bb)
  print("<BA>", cor_BA)
  
  # S = cor_AB + cor_aB - cor_Ab + cor_ab + cor_aA - cor_Bb + cor_BA

  # S = cor_AB + cor_Ab + cor_aB - cor_ab + cor_aA + cor_Bb + cor_BA
  # S = cor_AB + cor_Ab + cor_aB - cor_ab

  # どのエラー率でも、50まではどの相関値も絶対値にすることで、どの値が負になっても対応できるようにする
  S = np.abs(cor_AB) + np.abs(cor_Ab) + np.abs(cor_aB) + np.abs(cor_ab)
  if (error_rate >= 0.5) and (target_fid == 'X'):
    print("Xのエラー率を変更")
    # cor_aBを負にする
    S = np.abs(cor_AB + cor_Ab - cor_aB + cor_ab)

  if (error_rate >= 0.5) and (target_fid == 'Y'):
    print("Yのエラー率を変更")
    # cor_abを負にする
    S = np.abs(cor_AB + cor_Ab - cor_aB - cor_ab)

  if (error_rate >= 0.5) and (target_fid == 'Z'):
    print("Zのエラー率を変更")
    # cor_aBを負にする
    S = np.abs(cor_AB + cor_Ab - cor_aB + cor_ab)

  # 標準偏差を導出
  # 平均値を求める
  avg =  (total_AB + total_Ab + total_AA + total_aB + total_ab + total_aA + total_BB + total_Bb + total_BA) / 8
  print("avg", avg)
  print("total_AB", total_AB)
  print("total_Ab", total_Ab)
  print("total_AA", total_AA)
  print("total_aB", total_aB)
  print("total_ab", total_ab)
  print("total_aA", total_aA)
  print("total_BB", total_BB)
  print("total_Bb", total_Bb)
  print("total_BA", total_BA)
  # 偏差を求めて二乗
  # 上の和をデータの総数で割る
  sum_der = (total_AB - avg) ** 2 + (total_Ab - avg) ** 2 + (total_AA - avg) ** 2 + (total_aB - avg) ** 2 + (total_ab - avg) ** 2 + (total_aA - avg) ** 2 + (total_BB - avg) ** 2 + (total_Bb - avg) ** 2 + (total_BA - avg) ** 2
  print("sum_der", sum_der)
  # avg =  (total_AB + total_Ab + total_AA + total_aB + total_ab + total_aA + total_BB + total_Bb + total_BA) / 9
  avg_sum_der = sum_der / 9
  print("avg_sum_der", avg_sum_der)
  # 上の値の平方根を求める
  std_der = np.sqrt(avg_sum_der)

  return [round(S, 3), std_der]
   