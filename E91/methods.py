#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import fixedVecs as fiv
# from dm_methods import Dm 

def defBits():
  print("bits? ", end="")
  bits = int(input())

  return bits

def defEveflag():
  while True:
    print("eve?(y or n) ", end="")
    eve_flag = input()

    if (eve_flag == 'y') or (eve_flag == 'n'):
      return eve_flag

def defEverate(eve_flag):
  if eve_flag == 'n': return
  while True:
    print("eve rate?(int) ", end="")
    eve_rate = int(input())

    if (0 <= eve_rate) and (eve_rate <= 100):
      return eve_rate

def selectError():
  while True:
    print("Which error to change(X or Y or Z) ?", end="")
    target_fid = input()

    if (target_fid == 'X') or (target_fid == 'Y') or (target_fid == 'Z'):
      return target_fid

def defFidChangeValue():
  print("How many error to change(int) ?", end="")
  fid_change_value = int(input())

  return fid_change_value

def defFid(target_fid, fid_change_value, error_rate):
  fidelity = {
    'I': 0,
    'X': 0,
    'Y': 0,
    'Z': 0,
  }
  
  if target_fid == 'X':
    fidelity = {
      'I': 1 - error_rate,
      'X': error_rate,
      'Y': 0,
      'Z': 0,
    }
  elif target_fid == 'Y':
    fidelity = {
      'I': 1 - error_rate,
      'X': 0,
      'Y': error_rate,
      'Z': 0,
    }
  elif target_fid == 'Z':
    fidelity = {
      'I': 1 - error_rate,
      'X': 0,
      'Y': 0,
      'Z': error_rate,
    }

  return fidelity

def defineEveDm(eve_result):
  if eve_result == "00":
    dm_actual = np.kron(fiv.mesOps['0'], fiv.mesOps['0'])
  elif eve_result == "01":
    dm_actual = np.kron(fiv.mesOps['0'], fiv.mesOps['1'])
  elif eve_result == "10":
    dm_actual = np.kron(fiv.mesOps['1'], fiv.mesOps['0'])
  elif eve_result == "11":
    dm_actual = np.kron(fiv.mesOps['1'], fiv.mesOps['1'])

  return dm_actual

# If same basis is selected, secret key is generated
def deriveSeretKey(results):
  success_keys = []
  for i in range(len(results)):
    if (results[i]["base"] == "AA") or (results[i]["base"] == "BB"):
      if results[i]["result"] == "00":
        shared_bit = 0
      elif results[i]["result"] == "11":
        shared_bit = 1
      else:
        # in case of noise
        # print(i+1, "はノイズの影響を受けている？？？？？")
        shared_bit = 'X'

      success_keys.append({
        "index": i + 1,
        "base": results[i]["base"],
        "result": results[i]["result"],
        "shared_bit": shared_bit
      })

  return success_keys

def deriveFailedSecretKey(results):
  failed_keys = []
  for i in range(len(results)):
    if (results[i]["base"] != "AA") and (results[i]["base"] != "BB"):

      failed_keys.append({
        "index": i + 1,
        "base": results[i]["base"],
        "result": results[i]["result"],
        "shared_bit": 'F'
      })

  return failed_keys
