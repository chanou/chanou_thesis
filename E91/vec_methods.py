#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
np.set_printoptions(suppress=True)

def deriveDagger(operator):
  op_dagger = np.conjugate(operator.T)

  return op_dagger

# derive basis change(rotation) operator
def deriveRotateBsOp(operatorA, operatorB):
  rotateBsOp = np.kron(operatorA, operatorB)

  return rotateBsOp

# dm after rotated
def rotateDm(operator, dm):
  operator_d = deriveDagger(operator)
  process = np.dot(operator, dm)
  dm_r = np.dot(process, operator_d)

  return dm_r

# derive probability correspondint to measurement result
def deriveProb(operatorA, operatorB, dm):
  mes = np.kron(operatorA, operatorB)
  process = np.dot(deriveDagger(mes), mes)
  process = np.dot(process, dm)
  prob = np.trace(process).real

  return prob
