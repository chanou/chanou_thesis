#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import random
import fixedVecs as fiv
import vec_methods as vmes
import measurement as mes
import methods as mt
from tqdm import tqdm
# from dm_methods import Dm 

# Display up two decimal places
# np.set_printoptions(precision=2)

# Display decimal, NOT exponential
np.set_printoptions(suppress=True)

def E91(bits, fidelity, target_fid, error_rate, eve_rate):
  bells = []
  results = []
  keys = []
  
  # if index equals eve_indexes, there is eavesdropper
  if eve_rate:
    eve_num = int(bits * eve_rate)
    eve_indexes = random.sample(range(bits), eve_num)
    # print("eveを発する箇所", eve_indexes)
    # print("eveの個数", len(eve_indexes))
  else:
    eve_indexes = False

  print("deriving S value... ")
  for i in tqdm(range(bits)):

    dm_actual = fidelity['I'] * fiv.dm_I + fidelity['X'] * fiv.dm_X + fidelity['Y'] * fiv.dm_Y + fidelity['Z'] * fiv.dm_Z

    # FIXME: 必要なrotatedDmsのみ求めたい
    rotatedDms = fiv.defineRotateBsOp(dm_actual)

    if eve_indexes:
      # if index equals eve_indexes, there is eavesdropper
      if i in eve_indexes:
        # print(i+1,'でeveを発生させるよ')
        eve_base = np.random.choice(
          ["AB", "Ab", "AA", "aB", "ab", "aA", "BB", "Bb", "BA"],
          p = [
            1/9,
            1/9,
            1/9,
            1/9,
            1/9,
            1/9,
            1/9,
            1/9,
            1/9,
            ]
        )
        # print('eveが測定する場合のbase', eve_base)
        eve_probs = {
          "P00": vmes.deriveProb(fiv.mesOps['0'], fiv.mesOps['0'], rotatedDms[eve_base]),
          "P01": vmes.deriveProb(fiv.mesOps['0'], fiv.mesOps['1'], rotatedDms[eve_base]),
          "P10": vmes.deriveProb(fiv.mesOps['1'], fiv.mesOps['0'], rotatedDms[eve_base]),
          "P11": vmes.deriveProb(fiv.mesOps['1'], fiv.mesOps['1'], rotatedDms[eve_base])
        }
        # print('eveが測定する場合のprobs', eve_probs)

        eve_result = np.random.choice(
          ["00", "01", "10", "11"],
          p = [
            eve_probs["P00"],
            eve_probs["P01"],
            eve_probs["P10"],
            eve_probs["P11"]
          ]
        )
        # print("eveが測定した結果", eve_result)

        # eveが測定した結果に伴い、古典相関になる！！
        dm_actual = mt.defineEveDm(eve_result)
        # print("eveが測定したことによるdm\n", dm_actual)

    # dm_actual = fidelity['I'] * fiv.dm_I + fidelity['X'] * fiv.dm_X + fidelity['Y'] * fiv.dm_Y + fidelity['Z'] * fiv.dm_Z

    # 確認
    # if eve_indexes:
    #   if i in eve_indexes:
        # print('dm_actualについて\n', dm_actual)

    rotatedDms = fiv.defineRotateBsOp(dm_actual)

    # 確認
    # if eve_indexes:
    #   if i in eve_indexes:
        # print("rotatedDmsについていいいいいいい\n", rotatedDms)
    
    base = np.random.choice(
        ["AB", "Ab", "AA", "aB", "ab", "aA", "BB", "Bb", "BA"],
        p = [
          1/9,
          1/9,
          1/9,
          1/9,
          1/9,
          1/9,
          1/9,
          1/9,
          1/9,
          ]
      )

    # 同じ基底のみのテスト
    # base = np.random.choice(
    #     ["AA", "BB"],
    #     p = [0.5, 0.5]
    #   )

    # print(base)
    # print(bells[i])
    # print(rotatedDms)

    # FIXME:必要なrotatedDmsのみ求める
    probs = {
      "P00": vmes.deriveProb(fiv.mesOps['0'], fiv.mesOps['0'], rotatedDms[base]),
      "P01": vmes.deriveProb(fiv.mesOps['0'], fiv.mesOps['1'], rotatedDms[base]),
      "P10": vmes.deriveProb(fiv.mesOps['1'], fiv.mesOps['0'], rotatedDms[base]),
      "P11": vmes.deriveProb(fiv.mesOps['1'], fiv.mesOps['1'], rotatedDms[base])
    }

    result = np.random.choice(
        ["00", "01", "10", "11"],
        p = [
          probs["P00"],
          probs["P01"],
          probs["P10"],
          probs["P11"]
        ]
      )

    # if eve_indexes:
      # shotずつに選んだ結果などの確認
      # for i in eve_indexes:
        # print(i+1,'でeveを発生させるよ')
        # print("\n", i + 1, "bitめの結果")
        # print("選んだ基底", base)
        # print("probsの確認", probs)
        # print("測定結果", result)
        # print("============> fine\n")

    results.append(
      {
        "base": base,
        "result": result
      }
    )

  # shotずつに選んだ結果一覧の確認
  # for i in range(len(results)):
  #   print(results[i]["base"])

  # print(results)
  success_keys = mt.deriveSeretKey(results)
  failed_keys = mt.deriveFailedSecretKey(results)

  # derive S value
  results_for_S = mes.deriveS(results, target_fid, error_rate)
  S = results_for_S[0]
  hyoujunhensa = results_for_S[1]
  print("S value", S)

  # for i in range(len(success_keys)):
    # print("同じ基底を選んだ場合", success_keys[i])

  # 異なる基底を選んだ場合のみでS valueを導出する必要がある！！
  # for i in range(len(failed_keys)):
    # print("異なる基底を選んだ場合", failed_keys[i])

  # generate secret key
  print("\n<=======shared key=======>")
  for i in range(len(success_keys)):
    print(success_keys[i]["shared_bit"], end="")

  total_fail = 0
  for i in range(len(success_keys)):
    if success_keys[i]["shared_bit"] == 'X':
      total_fail = total_fail + 1

  total_success = 0
  total_success = len(success_keys) - total_fail
  
  success_rate = 0
  success_rate = total_success / len(results)
  
  print("\n\n<=======result=======>")
  print("-S value\n", S)
  print("-total shared success bits\n", total_success)
  print("-total shared falied bits\n", total_fail)
  print("-total trial bits\n", len(results))
  print("-shared success rate\n", success_rate)
  print("今回の標準偏差", hyoujunhensa)

  # return success_rate
  # return S
  return {
    "S_value": S,
    "success_rate": success_rate
    }