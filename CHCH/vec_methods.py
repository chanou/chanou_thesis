#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import fixedVecs as fiv
np.set_printoptions(suppress=True)

def deriveDagger(operator):
  op_dagger = np.conjugate(operator.T)

  return op_dagger

def deriveZMesOp(operator):
  ZMesOp = np.dot(operator, deriveDagger(operator))

  return ZMesOp

def deriveRotateBsOp(operatorA, operatorB):
  rotateBsOp = np.kron(operatorA, operatorB)

  return rotateBsOp

def rotateDm(operator, dm):
  operator_d = deriveDagger(operator)
  process = np.dot(operator, dm)
  dm_r = np.dot(process, operator_d)

  return dm_r

def deriveProb(operatorA, operatorB, dm):
  mes = np.kron(operatorA, operatorB)
  process = np.dot(deriveDagger(mes), mes)
  process = np.dot(process, dm)
  prob = np.trace(process).real

  return prob

def defineFid():
  while True:
    print("I fidelity? ", end="")
    I = float(input())

    print("X fidelity? ", end="")
    X = float(input())

    print("Y fidelity? ", end="")
    Y = float(input())
  
    print("Z fidelity? ", end="")
    Z = float(input())

    if I + X + Y + Z == 1.0:
      fidelity = {
        'I': I,
        'X': X,
        'Y': Y,
        'Z': Z,
      }
      return fidelity
      break
  