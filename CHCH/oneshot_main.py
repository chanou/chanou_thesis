#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import random
import fixedVecs as fiv
import vec_methods as vmes
import measurement as mes
from tqdm import tqdm
from matplotlib import pyplot
# from dm_methods import Dm 

# Display decimal, NOT exponential
np.set_printoptions(suppress=True)

# Display up two decimal places
# np.set_printoptions(precision=2)

if __name__ == "__main__":
  print('\n#####This is Bell test sim by chanou#####')
  print("bits? ", end="")
  bits = int(input())

  # print("\ncalculation base=======>")
  # print("-|0>\n", fiv.calBase['0'])
  # print("-|1>\n", fiv.calBase['1'])

  # print("\nZ measurement operator=======>")
  # print("-|0><0|\n", fiv.mesOps['0'])
  # print("-|1><1|\n", fiv.mesOps['1'])

  # # AB
  # print("\nprobability=======>AB")
  # print('P00\n', vmes.deriveProb(fiv.mesOps['0'], fiv.mesOps['0'], fiv.rotatedDms["AB"]))
  # print('P01\n', vmes.deriveProb(fiv.mesOps['0'], fiv.mesOps['1'], fiv.rotatedDms["AB"]))
  # print('P10\n', vmes.deriveProb(fiv.mesOps['1'], fiv.mesOps['0'], fiv.rotatedDms["AB"]))
  # print('P11\n', vmes.deriveProb(fiv.mesOps['1'], fiv.mesOps['1'], fiv.rotatedDms["AB"]))

  # # Ab
  # print("\nprobability=======>Ab")
  # print('P00\n', vmes.deriveProb(fiv.mesOps['0'], fiv.mesOps['0'], fiv.rotatedDms["Ab"]))
  # print('P01\n', vmes.deriveProb(fiv.mesOps['0'], fiv.mesOps['1'], fiv.rotatedDms["Ab"]))
  # print('P10\n', vmes.deriveProb(fiv.mesOps['1'], fiv.mesOps['0'], fiv.rotatedDms["Ab"]))
  # print('P11\n', vmes.deriveProb(fiv.mesOps['1'], fiv.mesOps['1'], fiv.rotatedDms["Ab"]))

  # # aB
  # print("\nprobability=======>aB")
  # print('P00\n', vmes.deriveProb(fiv.mesOps['0'], fiv.mesOps['0'], fiv.rotatedDms["aB"]))
  # print('P01\n', vmes.deriveProb(fiv.mesOps['0'], fiv.mesOps['1'], fiv.rotatedDms["aB"]))
  # print('P10\n', vmes.deriveProb(fiv.mesOps['1'], fiv.mesOps['0'], fiv.rotatedDms["aB"]))
  # print('P11\n', vmes.deriveProb(fiv.mesOps['1'], fiv.mesOps['1'], fiv.rotatedDms["aB"]))

  # # ab
  # print("\nprobability=======>ab")
  # print('P00\n', vmes.deriveProb(fiv.mesOps['0'], fiv.mesOps['0'], fiv.rotatedDms["ab"]))
  # print('P01\n', vmes.deriveProb(fiv.mesOps['0'], fiv.mesOps['1'], fiv.rotatedDms["ab"]))
  # print('P10\n', vmes.deriveProb(fiv.mesOps['1'], fiv.mesOps['0'], fiv.rotatedDms["ab"]))
  # print('P11\n', vmes.deriveProb(fiv.mesOps['1'], fiv.mesOps['1'], fiv.rotatedDms["ab"]))

  # 任意に変更していく角度
  # abitaly changing angle
  change_angle = 5

  S_values = []
  angles = []

  # TODO: for文の実行ごとにfidelityを変化する実装にしたほうがいいかも
  # define fidelity
  fidelity = vmes.defineFid()

  # NOT to increase the number of trials, implemented locally
  bells = []
  results = []
  
  angle = 45
  rotateOps = fiv.definerotateOps(angle)
  # print('rotateOpsの確認', rotateOps)

  print("deriving Svalue ...")
  for i in tqdm(range(bits)):

    dm_actual = fidelity['I'] * fiv.dm_I + fidelity['X'] * fiv.dm_X + fidelity['Y'] * fiv.dm_Y + fidelity['Z'] * fiv.dm_Z
    rotateBaseOps = fiv.definerotateBaseOps(rotateOps)
    # FIXME: 必要なrotatedDmsのみ求めたい
    rotatedDms = fiv.defineRotateBsOp(rotateBaseOps, dm_actual)
    
    base = np.random.choice(
      ["AB", "aB", "Ab", "ab"],
      p = [0.25, 0.25, 0.25, 0.25]
      )

    # FIXME:必要なrotatedDmsのみ求める
    probs = {
      "P00": vmes.deriveProb(fiv.mesOps['0'], fiv.mesOps['0'], rotatedDms[base]),
      "P01": vmes.deriveProb(fiv.mesOps['0'], fiv.mesOps['1'], rotatedDms[base]),
      "P10": vmes.deriveProb(fiv.mesOps['1'], fiv.mesOps['0'], rotatedDms[base]),
      "P11": vmes.deriveProb(fiv.mesOps['1'], fiv.mesOps['1'], rotatedDms[base])
    }

    result = np.random.choice(
      ["00", "01", "10", "11"],
      p = [
        probs["P00"],
        probs["P01"],
        probs["P10"],
        probs["P11"]
      ]
    )

    # これはなにを言ってんのかわからん
    # shotずつに選んだ結果などの確認
    # print("\n", i + 1, "bitめの結果")
    # print("選んだ基底", base)
    # print("測定結果", result)

    results.append(
      {
        "base": base,
        "result": result
      }
    )

    # shotずつに選んだ結果一覧の確認
    # print(results)

  # FIXME:1ビットしか送っていないっぽい
  results_for_S = mes.deriveS(results)
  # print('rotateOpsの確認', results_for_S)
  print("S value", results_for_S[0])
  print("今回の実験の標準偏差", results_for_S[1])

  # S_values.append(results_for_S[0])
  # angles.append(angle)
  
  # print(S_values)

