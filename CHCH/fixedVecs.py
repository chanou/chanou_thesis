#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import vec_methods as vmes

# Display decimal, NOT exponential
np.set_printoptions(suppress=True)

# Display up two decimal places
# np.set_printoptions(precision=2)

# caluculation base
calBase = {
  '0': np.array([1 , 0]).reshape(2, 1),
  '1': np.array([0, 1]).reshape(2, 1)
  }

# Z measurement operator
mesOps = {
  '0': vmes.deriveZMesOp(calBase['0']),
  '1': vmes.deriveZMesOp(calBase['1'])
}

# density matrix
# TODO: dm_methods.pyで式として定義する
dm_I = np.array([
    [0.5, 0, 0, 0.5],
    [0, 0, 0, 0],
    [0, 0, 0, 0],
    [0.5, 0, 0, 0.5],
  ])

# もつれていないパター完全混合
# dm = np.array([
#   [0.5, 0, 0, 0],
#   [0, 0, 0, 0],
#   [0, 0, 0, 0],
#   [0, 0, 0, 0.5],
# ])

dm_X = np.array([
  [0, 0, 0, 0],
  [0, 0.5, 0.5, 0],
  [0, 0.5, 0.5, 0],
  [0, 0, 0, 0],
])

dm_Z = np.array(
  [
    [0.5, 0, 0, -0.5],
    [0, 0, 0, 0],
    [0, 0, 0, 0],
    [-0.5, 0, 0, 0.5],
  ]
)

dm_Y = np.array([
  [0, 0, 0, 0],
  [0, 0.5, -0.5, 0],
  [0, -0.5, 0.5, 0],
  [0, 0, 0, 0],
])

def rotateY(theta, vec):
  rotate = np.array([
    [np.cos(theta/2), -np.sin(theta/2)],
    [np.sin(theta/2), np.cos(theta/2)]
  ])
  rotatedVec = np.dot(rotate, vec)

  return rotatedVec

def deriveRotateBsOp(theta, vec):
  theta_minus = -(np.pi - theta)
  plusvec = rotateY(theta, vec)
  minusvec = rotateY(theta_minus, vec)
  
  A = plusvec[0][0]
  B = plusvec[1][0]
  Ad = minusvec[0][0]
  Bd = minusvec[1][0]

  rotateBaseOp = np.array([
    [-Bd / (Ad * B - A * Bd), Ad / (Ad * B - A * Bd)],
    [- B / (A * Bd - Ad * B), A / (A * Bd -  Ad * B)]
  ])

  # print("9283479283498237474798", rotateBaseOp)
  return rotateBaseOp

def definerotateOps(angle):
  # angle = 45
  print("========>角度を変更", angle)
  angle = float(angle/180) * np.pi

  # rotation operator
  rotateOps = {
    # alice:A 0度 Z測定
    'A': np.array([
      [1, 0],
      [0, 1]
    ]),
    # alice:a 90度 X測定
    'a': deriveRotateBsOp(np.pi/2, calBase["0"]),
    # bob:B 任意に角度を変更
    'B': deriveRotateBsOp(angle, calBase["0"]),
    # bob:b 任意に角度を変更
    'b': deriveRotateBsOp(np.pi - angle, calBase["0"])
    }

  # print("9283479283498237474798", rotateOps['B'])
  return rotateOps

def definerotateBaseOps(rotateOps):
  # rotate BASE operator
  rotateBaseOps = {
      "AB": vmes.deriveRotateBsOp(rotateOps['A'], rotateOps['B']),
      "aB": vmes.deriveRotateBsOp(rotateOps['a'], rotateOps['B']),
      "Ab": vmes.deriveRotateBsOp(rotateOps['A'], rotateOps['b']),
      "ab": vmes.deriveRotateBsOp(rotateOps['a'], rotateOps['b'])
    }

  # print("9283479283498237474798", rotateBaseOps['aB'])
  return rotateBaseOps

# implement as a function, for the case of dm with errors
def defineRotateBsOp(rotateBaseOps, dm):

  rotatedDms = {
  "AB": vmes.rotateDm(rotateBaseOps["AB"], dm),
  "Ab": vmes.rotateDm(rotateBaseOps["Ab"], dm),
  "aB": vmes.rotateDm(rotateBaseOps["aB"], dm),
  "ab": vmes.rotateDm(rotateBaseOps["ab"], dm)
  }

  # print("9283479283498237474798", rotatedDms['aB'])
  return rotatedDms
