#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np

def deriveS(results):
  # print('生えおとこ', results)
  total_AB = 0
  s_AB_00 = 0
  s_AB_01 = 0
  s_AB_10 = 0
  s_AB_11 = 0

  total_aB = 0
  s_aB_00 = 0
  s_aB_01 = 0
  s_aB_10 = 0
  s_aB_11 = 0

  total_Ab = 0
  s_Ab_00 = 0
  s_Ab_01 = 0
  s_Ab_10 = 0
  s_Ab_11 = 0

  total_ab = 0
  s_ab_00 = 0
  s_ab_01 = 0
  s_ab_10 = 0
  s_ab_11 = 0

  for i in range(len(results)):

    if results[i]["base"] == "AB":
      if results[i]["result"] == "00":
        total_AB = total_AB + 1
        s_AB_00 = s_AB_00 + 1
      elif results[i]["result"] == "01":
        total_AB = total_AB + 1
        s_AB_01 = s_AB_01 - 1
      elif results[i]["result"] == "10":
        total_AB = total_AB + 1
        s_AB_10 = s_AB_10 - 1
      elif results[i]["result"] == "11":
        total_AB = total_AB + 1
        s_AB_11 = s_AB_11 + 1

    elif results[i]["base"]  == "aB":
      if results[i]["result"] == "00":
        total_aB = total_aB + 1
        s_aB_00 = s_aB_00 + 1
      elif results[i]["result"] == "01":
        total_aB = total_aB + 1
        s_aB_01 = s_aB_01 - 1
      elif results[i]["result"] == "10":
        total_aB = total_aB + 1
        s_aB_10 = s_aB_10 - 1
      elif results[i]["result"] == "11":
        total_aB = total_aB + 1
        s_aB_11 = s_aB_11 + 1

    elif results[i]["base"] == "Ab":
      if results[i]["result"] == "00":
        total_Ab = total_Ab + 1
        s_Ab_00 = s_Ab_00 + 1
      elif results[i]["result"] == "01":
        total_Ab = total_Ab + 1
        s_Ab_01 = s_Ab_01 - 1
      elif results[i]["result"] == "10":
        total_Ab = total_Ab + 1
        s_Ab_10 = s_Ab_10 - 1
      elif results[i]["result"] == "11":
        total_Ab = total_Ab + 1
        s_Ab_11 = s_Ab_11 + 1

    elif results[i]["base"] == "ab":
      if results[i]["result"] == "00":
        total_ab = total_ab + 1
        s_ab_00 = s_ab_00 + 1
      elif results[i]["result"] == "01":
        total_ab = total_ab + 1
        s_ab_01 = s_ab_01 - 1 
      elif results[i]["result"] == "10":
        total_ab = total_ab + 1
        s_ab_10 = s_ab_10 - 1
      elif results[i]["result"] == "11":
        total_ab = total_ab + 1
        s_ab_11 = s_ab_11 + 1

  print("total AB\n", total_AB)
  print("total Ab\n", total_Ab)
  print("total aB\n", total_aB)
  print("total ab\n", total_ab)
  print("trial count\n", total_AB + total_Ab + total_aB + total_ab)

  print("\nresult=======>")
  print("AB", total_AB, s_AB_00, s_AB_01, s_AB_10, s_AB_11)
  cor_AB = (s_AB_00 + s_AB_01 + s_AB_10 + s_AB_11) / total_AB
  print("<AB>", cor_AB)

  print("aB", total_aB, s_aB_00, s_aB_01, s_aB_10, s_aB_11)
  cor_aB = (s_aB_00 + s_aB_01 + s_aB_10 + s_aB_11) / total_aB
  print("<aB>", cor_aB)

  print("Ab", total_Ab, s_Ab_00, s_Ab_01, s_Ab_10, s_Ab_11)
  cor_Ab = (s_Ab_00 + s_Ab_01 + s_Ab_10 + s_Ab_11) / total_Ab
  print("<Ab>", cor_Ab)

  print("ab", total_ab, s_ab_00, s_ab_01, s_ab_10, s_ab_11)
  cor_ab = (s_ab_00 + s_ab_01 + s_ab_10 + s_ab_11) / total_ab
  print("<ab>", cor_ab)

  print("\nresult=======>")

  # S = cor_AB + cor_aB - cor_Ab + cor_ab
  # S = cor_AB + cor_aB + cor_Ab - cor_ab

  # 絶対値にすることで、どの値が負になっても対応できるようにする
  S = np.abs(cor_AB) + np.abs(cor_aB) + np.abs(cor_Ab) + np.abs(cor_ab)

  # 標準偏差を導出
  # 平均値を求める
  avg =  (total_AB + total_aB + total_Ab + total_ab) / 4
  # 偏差を求めて二乗
  # 上の和をデータの総数で割る
  sum_der = (total_AB - avg) ** 2 + (total_aB - avg) ** 2 + (total_Ab - avg) ** 2 + (total_ab - avg) ** 2
  avg_sum_der = sum_der / 4
  # 上の値の平方根を求める
  std_der = np.sqrt(avg_sum_der)
  
  return [round(S, 3), std_der]
